This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/C6A1BA98DF556356352087725973A03F465437CC) to [this Gitlab account](https://gitlab.com/hjpotter92). For details check out https://docs.keyoxide.org/advanced/openpgp-proofs/

[Verifying my OpenPGP key: openpgp4fpr:C6A1BA98DF556356352087725973A03F465437CC]
